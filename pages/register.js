import { Component } from 'react';
import {
  Layout,
  Button,
  Form,
  Input,
  Col,
  Card,
  Row,
  Space,
  DatePicker,
} from 'antd';
import { Content } from 'antd/lib/layout/layout';
import MenuHeader from './components/MenuHeader';
import Title from 'antd/lib/typography/Title';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

class Register extends Component {
  render = () => {
    return (
      <Layout {...layout}>
        <MenuHeader />
        <Content>
          <Row>
            <Col span={20} offset={2}>
              <Card>
                <Title>Cadastro</Title>

                <Form>
                  <Form.Item label="Nome">
                    <Input></Input>
                  </Form.Item>
                  <Form.Item label="Data de nascimento">
                    <DatePicker></DatePicker>
                  </Form.Item>
                  <Form.Item label="Telefone">
                    <Input></Input>
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" htmlType="submit">
                      Cadastrar
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </Content>
      </Layout>
    );
  };
}

export default Register;
