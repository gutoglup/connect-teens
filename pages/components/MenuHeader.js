import { Component } from 'react';
import { Layout, Button, Menu } from 'antd';
import { Content, Header } from 'antd/lib/layout/layout';

class MenuHeader extends Component {
  render = () => {
    return (
      <Header>
        <Menu theme="dark" mode="horizontal">
          <Menu.Item key="1">Ranking</Menu.Item>
          <Menu.Item key="2">
            <a href="/register">Cadastro</a>
          </Menu.Item>
        </Menu>
      </Header>
    );
  };
}

export default MenuHeader;
