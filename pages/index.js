import { Component } from 'react';
import { Layout } from 'antd';
import { Content } from 'antd/lib/layout/layout';
import MenuHeader from './components/MenuHeader';
import Title from 'antd/lib/typography/Title';

class Home extends Component {
  render = () => {
    return (
      <Layout className="layout">
        <MenuHeader></MenuHeader>
        <Content>
          <Title>Connect Teens</Title>
        </Content>
      </Layout>
    );
  };
}

export default Home;
